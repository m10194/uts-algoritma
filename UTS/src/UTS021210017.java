
import java.util.Scanner;

public class UTS021210017 {

    public static void main(String[] args) {
        String npm, nama, prodi;
        Scanner SC = new Scanner(System.in);
        System.out.print("Silahkan masukkan NAMA anda: ");
        nama = SC.nextLine();
        System.out.println("NAMA anda: " + nama);
        
        System.out.print("Silahkan masukkan NPM anda: ");
        npm = SC.nextLine();
        System.out.println("NPM anda: " + npm);
        
        System.out.print("Silahkan masukkan PRODI anda: ");
        prodi = SC.nextLine();
        System.out.println("PRODI anda: " + prodi);
        
        String pernyataan = null;
        Scanner sc = new Scanner(System.in);
        System.out.print("Jika nilai anda : ");
        int nilai = sc.nextInt();
        if (nilai >= 70) {
            pernyataan = "LULUS";
        } else if (nilai < 70) {
            pernyataan = "GAGAL";
        }
        System.out.println("Maka anda dinyatakan " + pernyataan);
    }
}
